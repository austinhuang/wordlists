An opinionated fork of the
[EFF Diceware wordlists](https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases).

## Properties

The following properties hold true:

* Each word has between three and ten characters
* No word is a prefix of another word on the same list
* Within the `words-4-uniq-3` list, no two words begin with the same three letters
* The length of each list is a power of `6` (`7776` for `words-5`, `1296` for `words-4` and
  `words-4-uniq-3`)
* Each list is sorted
* Each word consists only of lowercase letters (in particular, there are no hyphens)

The first five properties were preserved from the EFF wordlists. The last one was added by me in
this fork.

## Opinionated?

I am imperfect and biased. Many of the additions and removals I made are largely based on my
personal perception of particular words as well-known or obscure (and a few changes are just plain
arbitrary).

Feel free to submit a merge request to suggest further changes or open an issue to discuss…issues.
Or just create a fork of this fork. :)

## Enhancements over the EFF lists

The EFF lists can provide strong security, but offer some challenges to usability. In particular,
they contain:

* Alternative spellings of the same words: _bony_ and _boney_, _compactor_ and _compacter_, _sulfur_
  and _sulphur_, _yo-yo_ and _yoyo_…
* Non-standard compound words: _bargraph_, _fryingpan_, _videogame_, _zipfile_…
* Excessive variations on numbers: _ninth_ and _ninetieth_; _seventh_, _sevenfold_, _seventeen_, and
  _seventy_; _sixth_, _sixfold_, _sixteen_, _sixtieth_, _sixties_, and _sixtyfold_; …
* Words with hyphens: _drop-down_, _t-shirt_, _yo-yo_…
* Misspellings: _jalapeno_, _negligee_, _senorita_, _useable_, _worshiper_…
* Proper nouns: _chihuahua_, _dachshund_, _geiger_, _oxford_, _panama_, _santa_, _wikipedia_…
* Corporate names, brand names, and genericized trademarks (c'mon, EFF, really?): _ebay_,
  _facebook_, _frisbee_, _iphone_, _jeep_, _kleenex_, _linoleum_, _ritalin_, _skype_, _thermos_,
  _tupperware_, _valium_, _velcro_, _walmart_, _xbox_, _xerox_…

These issues could cause confusion, difficulty distinguishing multiple very similar words,
difficulty recalling the positions of word boundaries, or difficulty recalling exactly how to spell
a word. (Except perhaps the last two. Those I just didn't want to appear in my passphrases. >.> )

(There is also an odd omission of any words at all beginning with certain letter combinations (e.g.
`hi`, `lo`, `me`, `mi`, `no`, `so`, `we`…). Perhaps these were arbitrarily culled to bring the size
of the list down to 7776? This has not (yet?) been addressed.)
