#!/usr/bin/env perl
use strict;
use warnings;

# paste-dice-rolls.pl -- take a bare wordlist and add dice rolls on the left
#
# Usage: paste-dice-rolls.pl FILE
#
# FILE is assumed to be a wordlist (one word on each line). The lines will be counted (to determine
# how many dice are needed to use the list) and then output again prefixed with a roll of six-sided
# dice and a tab character. For example (with a typical 7776-word list):
#
# 11111	aardvark
# 11112	abacus
# 11113	abandoned
# ...
# 66666	zoom

use Fcntl;

my $DIE_SIDE_COUNT = 6;

sub print_rolls_and_words {
    my ($file, $length, $iterations, $prefix) = @_;
    $prefix //= "";
    if ($iterations <= 0) {
        my $line = <$file> // return;
        print("$prefix\t$line");
        return 1;
    }
    for (my $i = 1; $i <= $length; ++$i) {
        print_rolls_and_words($file, $length, $iterations - 1, "$prefix$i") or return;
    }
    return 1;
}

# Integer logarithm. Rounds _up._
#
# I'm not confident that floats are accurate enough, and even this naïve iterative approach should
# be reasonably fast, not that we need to go that fast anyway.
sub logi {
    my ($target_power, $base) = @_;
    return undef if ($target_power <= 0 || $base <= 0);
    my $exponent = 0;
    ++$exponent while $base ** $exponent < $target_power;
    return $exponent;
}

my ($file_name) = @ARGV;
open(my $file, "<:encoding(ascii)", $file_name) or die "couldn't open $file_name";
my $line_count = 0;
++$line_count while (my $line = <$file>);
seek($file, 0, Fcntl::SEEK_SET) or die "couldn't seek in $file_name";
# No lines? No output. (Specialcased here to avoid trying to take the log of 0)
exit if $line_count == 0;
my $die_count = logi($line_count, $DIE_SIDE_COUNT) // die;
print_rolls_and_words($file, $DIE_SIDE_COUNT, $die_count);
